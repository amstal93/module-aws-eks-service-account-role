0.1.1
=====

* fix: permit aws provider > 3
* chore: bump pre-commit hooks

0.1.0
=====

* feat: initial release
